FROM jenkins/jenkins:latest
USER root
RUN apt update
RUN apt install -y wget git apt-transport-https lsb-release ca-certificates curl
RUN curl -sSL -o /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
RUN sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
RUN apt update
RUN apt install -y         \
 	php7.4             \
	php7.4-cli         \
	php7.4-common      \
	php7.4-curl        \
	php7.4-imap        \
	php7.4-json        \
	php7.4-mysql       \
	php7.4-opcache     \
	php7.4-pgsql       \
	php7.4-readline    \
	php7.4-xml         \ 
	php7.4-zip         \
	php7.4-mbstring    \
	php7.4-intl        \
	php7.4-bcmath      \
	php7.4-gd


# Install Node
ARG node_version=14.15.1
RUN wget --quiet https://nodejs.org/dist/v${node_version}/node-v${node_version}-linux-x64.tar.xz
RUN tar xf node-v${node_version}-linux-x64.tar.xz
ENV PATH="/node-v"${node_version}"-linux-x64/bin:"${PATH}

# Install Composer
ARG composer_version=1.10.17
RUN wget https://getcomposer.org/download/${composer_version}/composer.phar
RUN mkdir -pv /composer/bin ; mv composer.phar /composer/bin/composer ; chmod 755 /composer/bin/composer 
ENV PATH="/composer/bin:"${PATH}


ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/jenkins.sh"]
